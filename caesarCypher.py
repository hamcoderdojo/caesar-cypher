'''
a cool, collaborative, first project @ ham coderdojo
a program that encrypts or decrypts messages
'''

alphabet = 'abcdefghijklmnopqrstuvwxyz'

# function which encrypts a message
# takes a string and returns encrypted string
# string is encrypted using caesar cypher algorihtm
# each character is replace with another, 13 characters along

def encrypt(message):
    encrypted = ''
    for letter in message:
        position = alphabet.index(letter)
        new_position = (position + 13) % 26
        new_letter = alphabet[new_position]
        encrypted = encrypted + new_letter
    return encrypted

# function which decrypts a message
# takes an encrypted string and returns a decrpyted string
# string is assumed caesar cypher encrpyted
# each letter is replaced with 13th before

def decrypt(message):
    decrypted = ''
    for letter in message:
        position = alphabet.index(letter)
        new_position = position - 13 % 26
        new_letter = alphabet[new_position]
        decrypted = decrypted + new_letter
    return decrypted
